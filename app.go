package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path"
	"strconv"
)

type task struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

var usr *user.User
var err error
var taskFile string = ".linux-terminal-tasks.json"
var filePath string

func main() {
	usr, err = user.Current()
	filePath = path.Join(usr.HomeDir, taskFile)

	if err != nil {
		log.Fatal(err)
	}

	args := os.Args[1:]

	if args[0] == "-l" {
		mostrarLista()
	}

	if args[0] == "-ld" {
		mostrarListaDetallada()
	}

	if args[0] == "-a" {
		if len(args) == 3 {
			anadirTarea(args[1], args[2])
		} else {
			fmt.Println("Parámetros incorrectos")
		}
	}

	if args[0] == "-d" {
		if len(args) == 2 {
			n, err := strconv.Atoi(args[1])
			if err != nil {
				fmt.Println("Parámetros incorrectos")
				return
			}
			borrarTarea(n)
		} else {
			fmt.Println("Parámetros incorrectos")
		}
	}

	if args[0] == "-h" {
		fmt.Println("Linux terminal task #")
		fmt.Println("#####################")
		fmt.Println("-l -> Muestra un listado resumido de las tareas")
		fmt.Println("-ld -> Muestra un listado detallado de las tareas")
		fmt.Println("-a \"titulo\" \"descripcion\" -> Añade una tarea a la cola de la lista")
		fmt.Println("-d indice -> Borra la tarea cuyo índice coincida")
	}

}

func mostrarLista() {

	file, _ := ioutil.ReadFile(filePath)
	tasks := []task{}
	_ = json.Unmarshal([]byte(file), &tasks)

	for i, task := range tasks {
		fmt.Printf("%v.- Titulo: %v\n", i, task.Title)
	}
}

func mostrarListaDetallada() {
	file, _ := ioutil.ReadFile(filePath)
	tasks := []task{}
	_ = json.Unmarshal([]byte(file), &tasks)

	for i, task := range tasks {
		fmt.Printf("%v.- Titulo: %v\n", i, task.Title)
		fmt.Printf("%v.- Descripcion: %v\n", i, task.Description)
	}
}

func anadirTarea(titulo string, descripcion string) {
	nt := task{}
	nt.Title = titulo
	nt.Description = descripcion

	file, _ := ioutil.ReadFile(filePath)
	tasks := []task{}
	_ = json.Unmarshal([]byte(file), &tasks)

	tasks = append(tasks, nt)
	file2, _ := json.MarshalIndent(tasks, "", " ")
	err := ioutil.WriteFile(filePath, file2, 0644)
	if err != nil {
		fmt.Println("err", err)
	}
}

func borrarTarea(indice int) {

	file, _ := ioutil.ReadFile(filePath)
	tasks := []task{}
	_ = json.Unmarshal([]byte(file), &tasks)

	if len(tasks) <= indice {
		fmt.Println("La tarea no existe")
		return
	}

	newTasks := []task{}
	newTasks = append(newTasks, tasks[:indice]...)
	newTasks = append(newTasks, tasks[indice+1:]...)

	file2, _ := json.MarshalIndent(newTasks, "", " ")
	err := ioutil.WriteFile(filePath, file2, 0644)
	if err != nil {
		fmt.Println("error", err)
		return
	}
	fmt.Println("Tarea borrada")
}
